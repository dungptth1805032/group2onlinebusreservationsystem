﻿using Group2OnlineBusReservationSystem.Models;
using Group2OnlineBusReservationSystem.Models.AppModels;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Group2OnlineBusReservationSystem.Controllers
{
    [Authorize]
    public class StationsController : Controller
    {
        private G2OnlineBusTicketEntities db = DatabaseConnect.getInstance();
        public ActionResult Index()
        {
            return View(db.Stations.ToList());
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,description,lat,lng,created_at,modified_at")] Station station)
        {
            if (ModelState.IsValid)
            {
                db.Stations.Add(station);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(station);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Station station = db.Stations.Find(id);
            if (station == null)
            {
                return HttpNotFound();
            }
            return View(station);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,description,lat,lng,created_at,modified_at")] Station station)
        {
            if (ModelState.IsValid)
            {
                db.Set<Station>().AddOrUpdate(station);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(station);
        }
    }
}
