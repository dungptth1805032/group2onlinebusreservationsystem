﻿using Group2OnlineBusReservationSystem.Models;
using Group2OnlineBusReservationSystem.Models.AppModels;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Group2OnlineBusReservationSystem.Controllers
{
    [Authorize]
    public class PricesController : Controller
    {
        private G2OnlineBusTicketEntities db = DatabaseConnect.getInstance();
        public ActionResult Index()
        {
            return View(db.Prices.ToList());
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Create()
        {
            ViewBag.bus_type = new SelectList(db.BusTypes, "name", "name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,bus_type,price1,created_at,modified_at")] Price price)
        {
            if (ModelState.IsValid)
            {
                db.Prices.Add(price);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(price);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Price price = db.Prices.Find(id);
            if (price == null)
            {
                return HttpNotFound();
            }
            ViewBag.bus_type = new SelectList(db.BusTypes, "name", "name");
            return View(price);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,bus_type,price1,created_at,modified_at")] Price price)
        {
            if (ModelState.IsValid)
            {
                db.Set<Price>().AddOrUpdate(price);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(price);
        }
    }
}
