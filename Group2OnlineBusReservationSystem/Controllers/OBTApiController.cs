﻿using Group2OnlineBusReservationSystem.Models;
using Group2OnlineBusReservationSystem.Models.AppModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Group2OnlineBusReservationSystem.Controllers
{
    public class OBTApiController : ApiController
    {
        private G2OnlineBusTicketEntities db = DatabaseConnect.getInstance();
        public ICollection<StationResult> GetStations()
        {
            var result = new List<StationResult>();
            try
            {
                result = (
                    from s in db.Stations
                    where s.name != null && s.lat != null && s.lng != null
                    select new StationResult
                    {
                        name = s.name,
                        description = s.description,
                        lat = s.lat,
                        lng = s.lng
                    }).ToList();
            }
            catch { }
            return result;
        }

        public bool GetSeatStatus(int id, int journeyId, DateTime startDate)
        {
            var result = ClientLogicModels.IsSeatBooked(id, journeyId, startDate);
            if (!result)
            {
                try
                {
                    db.Seats.Find(id).expired_at = DateTime.Now.AddMinutes(5);
                    db.SaveChanges();
                }
                catch { }
            }
            return result;
        }

        public bool GetCancelSeatSatus(int id)
        {
            db.Seats.Find(id).expired_at = DateTime.Now;
            db.SaveChanges();
            return true;
        }
    }
}
