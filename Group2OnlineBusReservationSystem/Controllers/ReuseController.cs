﻿using Group2OnlineBusReservationSystem.Models;
using Group2OnlineBusReservationSystem.Models.AppModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Group2OnlineBusReservationSystem.Controllers
{
    public class ReuseController : Controller
    {
        private G2OnlineBusTicketEntities db = DatabaseConnect.getInstance();
        public ActionResult SearchJourney()
        {
            var stations = new List<string>();
            var busTypes = new List<string>();
            try
            {
                stations = (from s in db.Stations select s.name).ToList();
                busTypes = (from b in db.Buses select b.type).Distinct().ToList();
            }
            catch { }
            ViewBag.stations = stations;
            ViewBag.busTypes = busTypes;
            return PartialView("_SearchJourney");
        }

        public ActionResult SearchJourneyInline()
        {
            var stations = new List<string>();
            var busTypes = new List<string>();
            try
            {
                stations = (from s in db.Stations select s.name).ToList();
                busTypes = (from b in db.Buses select b.type).Distinct().ToList();
            }
            catch { }
            ViewBag.stations = stations;
            ViewBag.busTypes = busTypes;
            return PartialView("_SearchJourneyInline");
        }

        public ActionResult SeatGrid(List<List<SeatResult>> seatGrid, JourneyDetailResult journeyDetail)
        {
            ViewBag.seatGrid = seatGrid;
            ViewBag.journeyDetail = journeyDetail;
            return PartialView("_SeatGrid");
        }

        public ActionResult Cart()
        {
            return PartialView("_Cart");
        }
    }
}