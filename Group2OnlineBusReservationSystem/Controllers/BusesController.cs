﻿using Group2OnlineBusReservationSystem.Models;
using Group2OnlineBusReservationSystem.Models.AppModels;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Group2OnlineBusReservationSystem.Controllers
{
    [Authorize]
    public class BusesController : Controller
    {
        private G2OnlineBusTicketEntities db = DatabaseConnect.getInstance();
        public ActionResult Index()
        {
            var buses = db.Buses.Include(b => b.Employee);
            return View(buses.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bus bus = db.Buses.Find(id);
            if (bus == null)
            {
                return HttpNotFound();
            }
            ViewBag.bus = bus;
            return View(bus);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Create()
        {
            ViewBag.employee_id = new SelectList(db.Employees, "id", "name");
            ViewBag.type = new SelectList(db.BusTypes, "name", "name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,employee_id,type,license_plates,created_at,modified_at")] Bus bus)
        {
            if (ModelState.IsValid)
            {
                db.Buses.Add(bus);
                db.SaveChanges();
                var busId = bus.id;
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "A1" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "A2" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "A3" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "A4" });

                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "B1" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "B2" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "B3" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "B4" });

                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "C1" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "C2" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "C3" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "C4" });

                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "D1" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "D2" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "D3" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "D4" });

                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "E1" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "E2" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "E3" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "E3" });

                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "F1" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "F2" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "F3" });
                db.Seats.Add(new Seat { bus_id = busId, expired_at = DateTime.Now, name = "F4" });
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.employee_id = new SelectList(db.Employees, "id", "account_id", bus.employee_id);
            return View(bus);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bus bus = db.Buses.Find(id);
            if (bus == null)
            {
                return HttpNotFound();
            }
            ViewBag.employee_id = new SelectList(db.Employees, "id", "name", bus.employee_id);
            ViewBag.type = new SelectList(db.BusTypes, "name", "name", bus.type);
            return View(bus);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,employee_id,type,license_plates,created_at,modified_at")] Bus bus)
        {
            if (ModelState.IsValid)
            {
                db.Set<Bus>().AddOrUpdate(bus);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.employee_id = new SelectList(db.Employees, "id", "account_id", bus.employee_id);
            return View(bus);
        }
    }
}
