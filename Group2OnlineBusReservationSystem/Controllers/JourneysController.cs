﻿using Group2OnlineBusReservationSystem.Models;
using Group2OnlineBusReservationSystem.Models.AppModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Group2OnlineBusReservationSystem.Controllers
{
    [Authorize]
    public class JourneysController : Controller
    {
        private G2OnlineBusTicketEntities db = DatabaseConnect.getInstance();
        public ActionResult Index()
        {
            var journeys = db.Journeys.Include(j => j.Bus);
            return View(journeys.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Journey journey = db.Journeys.Find(id);
            if (journey == null)
            {
                return HttpNotFound();
            }
            return View(journey);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Create()
        {
            ViewBag.bus_id = new SelectList(db.Buses, "id", "license_plates");
            ViewBag.stations = db.Stations.ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection formData)
        {
            var routes = formData["hiddenField"].TrimEnd(',');
            var routesJson = JsonConvert.DeserializeObject<List<int>>(routes);

            var newJourney = new Journey();
            newJourney.bus_id = Int32.Parse(formData["bus_id"]);
            newJourney.type = formData["type"];
            var st = formData["start_time"];
            newJourney.start_time = Convert.ToDateTime(formData["start_time"]);
            newJourney.arrival_time = Convert.ToDateTime(formData["arrival_time"]);
            db.Journeys.Add(newJourney);
            foreach (var route in routesJson)
            {
                var newRoute = new Route();
                newRoute.journey_id = newJourney.id;
                newRoute.station_id = Int32.Parse(formData["inputStopStation" + route + ""]);
                newRoute.time_from_previous_station = TimeSpan.Parse(formData["inputTimeFLL" + route + ""]);
                newRoute.distance_from_previous_station = Int32.Parse(formData["inputDistanceFLL" + route + ""]);
                db.Routes.Add(newRoute);
                db.SaveChanges();
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Journey journey = db.Journeys.Find(id);
            if (journey == null)
            {
                return HttpNotFound();
            }
            return View(journey);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Journey journey = db.Journeys.Find(id);
            if (journey.BookingDetails.Count() > 0)
            {
                return RedirectToAction("Index");
            }
            else
            {
                var routesId = new List<int>();
                foreach (var route in journey.Routes)
                {
                    routesId.Add(route.id);
                }

                foreach (var routeId in routesId)
                {
                    db.Routes.Remove(db.Routes.Find(routeId));
                }

                db.Journeys.Remove(journey);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
        }
    }
}
