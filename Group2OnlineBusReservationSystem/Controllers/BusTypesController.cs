﻿using Group2OnlineBusReservationSystem.Models;
using Group2OnlineBusReservationSystem.Models.AppModels;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Group2OnlineBusReservationSystem.Controllers
{
    [Authorize]
    public class BusTypesController : Controller
    {
        private G2OnlineBusTicketEntities db = DatabaseConnect.getInstance();
        public ActionResult Index()
        {
            return View(db.BusTypes.ToList());
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,created_at,modified_at")] BusType busType)
        {
            if (ModelState.IsValid)
            {
                db.BusTypes.Add(busType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(busType);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BusType busType = db.BusTypes.Find(id);
            if (busType == null)
            {
                return HttpNotFound();
            }
            return View(busType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,created_at,modified_at")] BusType busType)
        {
            if (ModelState.IsValid)
            {
                db.Set<BusType>().AddOrUpdate(busType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(busType);
        }

        [Authorize(Roles = "Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BusType busType = db.BusTypes.Find(id);
            if (busType == null)
            {
                return HttpNotFound();
            }
            return View(busType);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BusType busType = db.BusTypes.Find(id);
            db.BusTypes.Remove(busType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
