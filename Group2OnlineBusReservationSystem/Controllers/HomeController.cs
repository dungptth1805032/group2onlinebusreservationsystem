﻿using Group2OnlineBusReservationSystem.Models;
using Group2OnlineBusReservationSystem.Models.AppModels;
using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Group2OnlineBusReservationSystem.Controllers
{
    public class HomeController : Controller
    {
        private G2OnlineBusTicketEntities db = DatabaseConnect.getInstance();
        public ActionResult Index()
        {
            var journeys = new List<Journey>();
            try
            {
                journeys = (from j in db.Journeys where j.start_time <= DateTime.Now select j).ToList();
            }
            catch { }
            ViewBag.journeys = journeys;
            return View();
        }

        [HttpPost]
        public ActionResult Index(JourneySearch searchQuery)
        {
            return RedirectToAction("SearchResult", new
            {
                id = 1,
                searchQuery.startLocation,
                searchQuery.endLocation,
                searchQuery.busType,
                searchQuery.startDate,
            });
        }

        public ActionResult SearchResult(int id, JourneySearch searchQuery)
        {
            var journeyDetails = ClientLogicModels.SearchForJourneys(searchQuery);
            ViewBag.journeyDetails = journeyDetails;
            ViewBag.startLocation = searchQuery.startLocation;
            ViewBag.endLocation = searchQuery.endLocation;
            return View();
        }

        public ActionResult SelectSeat(int id, JourneySearch searchQuery)
        {
            if (searchQuery.startLocation == null || searchQuery.endLocation == null || searchQuery.busType == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                try
                {
                    var journey = db.Journeys.Find(id);
                    var result = ClientLogicModels.GetJourneyDetail(journey, searchQuery);
                    var seatGrid = ClientLogicModels.GetSeatGrid(journey.id, searchQuery.startDate);
                    ViewBag.journey = result;
                    ViewBag.seatGrid = seatGrid;
                }
                catch { }
            }
            return View();
        }

        public ActionResult Checkout()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Checkout(FormCollection formData)
        {
            List<Ticket> tickets = ClientLogicModels.SaveBookingInformation(formData);
            var bookingId = tickets.FirstOrDefault().BookingDetail.id;
            if (formData["paymentOption"] == "paypal")
            {
                APIContext apiContext = PaypalConfiguration.GetAPIContext();
                var baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/Home/PaymentWithPayPal/" + bookingId + "/?";
                var guid = Convert.ToString((new Random()).Next(100000));
                var createdPayment = PaypalPaymentAction.CreatePayment(apiContext, baseURI + "guid=" + guid, tickets);
                var links = createdPayment.links.GetEnumerator();
                var paypalRedirectUrl = "";
                while (links.MoveNext())
                {
                    Links lnk = links.Current;
                    if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                    {
                        paypalRedirectUrl = lnk.href;
                    }
                }
                Session.Add(guid, createdPayment.id);
                return Redirect(paypalRedirectUrl);
            }
            else
            {
                return RedirectToAction("CheckoutResult", new { id = bookingId, status = true });
            }
        }

        public ActionResult PaymentWithPayPal(int id, string Cancel = null)
        {
            try
            {
                APIContext apiContext = PaypalConfiguration.GetAPIContext();
                var guid = Request.Params["guid"];
                string payerId = Request.Params["PayerID"];
                var executedPayment = PaypalPaymentAction.ExecutePayment(apiContext, payerId, Session[guid] as string);
                if (executedPayment.state.ToLower() != "approved")
                {
                    return RedirectToAction("CheckoutResult", new { id = id, status = false });
                }
            }
            catch
            {
                return RedirectToAction("CheckoutResult", new { id = id, status = false });
            }
            return RedirectToAction("CheckoutResult", new { id = id, status = true });
        }

        public ActionResult CheckoutResult(int id, bool status)
        {
            var bookingDetail = db.BookingDetails.Find(id);
            var seats = new List<Seat>();
            foreach (var ticket in bookingDetail.Tickets)
            {
                seats.Add(db.Seats.Find(ticket.seat_id));
            }
            ViewBag.bookingDetail = bookingDetail;
            ViewBag.seats = seats;
            ViewBag.isSuccess = status;

            if (status)
            {
                try
                {
                    GMailer.GmailUsername = "dungptth1805032@fpt.edu.vn";
                    GMailer.GmailPassword = "DungTien20001218-+--";
                    decimal totalPrice = 0;
                    StringBuilder htmlBody = new StringBuilder();
                    htmlBody.Append("<html><body>");
                    htmlBody.Append("<p>Dear Cusomer,</p>");
                    htmlBody.Append("<p>Here your booking information:</p>");
                    htmlBody.Append($"<strong>Booking ID: {bookingDetail.id.ToString()} </strong>");
                    htmlBody.Append($"<p>From {bookingDetail.from_location} at {bookingDetail.start_time} To {bookingDetail.to_location} at {bookingDetail.arrival_time} </p>");
                    htmlBody.Append($"<p>Bus {bookingDetail.Journey.Bus.license_plates}</p>");
                    htmlBody.Append("<table border='1'>");
                    htmlBody.Append("<tr><th>Ticket ID</th><th>Seat</th><th>Customer</th><th>CID</th><th>Price (VND)</th></tr>");
                    foreach (var t in bookingDetail.Tickets)
                    {
                        totalPrice += t.price;
                        var seat = db.Seats.Find(t.seat_id);
                        htmlBody.Append($"<tr><td>{t.id}</td><td>{seat.name}</td><td>{t.customer_name}</td><td>{t.citizenship_id}</td><td>{t.price}</td></tr>");
                    }
                    htmlBody.Append($"<tr><td colspan='4'></td><td>{totalPrice.ToString()}</td></tr>");
                    htmlBody.Append("</table>");
                    htmlBody.Append("</body></html>");

                    GMailer mailer = new GMailer();
                    mailer.ToEmail = bookingDetail.Customer.email;
                    mailer.Subject = "Booking information";
                    mailer.Body = htmlBody.ToString();
                    mailer.IsHtml = true;
                    mailer.Send();
                }
                catch { }
            }
            return View();
        }

        public ActionResult MyTickets()
        {
            ViewBag.noResult = true;
            return View();
        }

        [HttpPost]
        public ActionResult MyTickets(TicketsSearch searQuery)
        {
            try
            {
                var bookingDetail = db.BookingDetails.Where(
                    bd => bd.id == searQuery.bookingId
                    && bd.Customer.phone == searQuery.phone).FirstOrDefault();
                if (bookingDetail != null)
                {
                    var tickets = searQuery.ticketId != 0
                        ? bookingDetail.Tickets.Where(t => t.id == searQuery.ticketId)
                        : bookingDetail.Tickets;

                    ViewBag.bookingDetail = bookingDetail;
                    ViewBag.tickets = tickets;
                    ViewBag.noResult = false;
                }
                else
                {
                    ViewBag.noResult = true;
                }
            }
            catch { }
            ViewBag.bookingId = searQuery.bookingId;
            ViewBag.phone = searQuery.phone;
            return View();
        }

        [HttpPost]
        public ActionResult CancelTicket(FormCollection formData)
        {
            var bookingId = Convert.ToInt32(formData["bookingId"]);
            var ticketId = Convert.ToInt32(formData["ticketId"]);
            var phone = formData["phone"];
            try
            {
                var bookingDetail = db.BookingDetails.Where(
                    bd => bd.id == bookingId
                    && bd.Customer.phone == phone).FirstOrDefault();
                var ticket = db.Tickets.Find(ticketId);
                bookingDetail.PaymentDetail.refund += ticket.price;
                ticket.price = 0;
                db.SaveChanges();
            }
            catch { }
            return RedirectToAction("MyTickets");
        }
    }
}