﻿using Group2OnlineBusReservationSystem.Models;
using Group2OnlineBusReservationSystem.Models.AppModels;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Group2OnlineBusReservationSystem.Controllers
{
    [Authorize]
    public class CustomersController : Controller
    {
        private G2OnlineBusTicketEntities db = DatabaseConnect.getInstance();
        public ActionResult Index(string FName, string FCID)
        {
            var result = db.Customers.ToList();
            if (FName != null && FName != "")
            {
                result = result.Where(c => c.name == FName).ToList();
            }

            if (FCID != null && FCID != "")
            {
                result = result.Where(c => c.citizenship_id == FCID).ToList();
            }
            return View(result);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }
    }
}
