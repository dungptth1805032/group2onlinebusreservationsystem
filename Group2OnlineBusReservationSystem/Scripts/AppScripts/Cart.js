﻿window.expiredTicketRemove = () => {
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    seats = seats.filter((seat) => {
        const time = (new Date(seat.expireDate).getTime()) - (new Date().getTime());
        return time > 0 ? true : false;
    });
    localStorage.setItem("seats", JSON.stringify(seats));
}

window.setCountdown = (elementId, expireDate) => {
    const deadline = new Date(expireDate).getTime();
    const timer = setInterval(() => {
        const time = deadline - (new Date().getTime());
        if (time > 0) {
            const minutes = Math.floor((time % (1000 * 60 * 60)) / (1000 * 60));
            const seconds = Math.floor((time % (1000 * 60)) / 1000);
            const newElement = `${minutes}m ${seconds}s before expire`;
            try {
                document.getElementById(elementId).innerHTML = newElement;
            } catch {
                clearInterval(timer);
            }
        } else {
            document.getElementById(elementId).innerHTML = "Expire";
            cancelSeat(elementId.substring(6));
            clearInterval(timer);
        }
    }, 1000);
};

window.updateCart = () => {
    expiredTicketRemove();
    $("#cart").empty();
    const seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    if (seats.length < 1) {
        $("#cart").append(`<p class="text-danger mb-1">Your cart is empty</p>`);
        return;
    } else {
        seats.forEach((seat) => {
            const newElement = `
            <div class="cart-item mb-1">
                <strong>Seat: </strong>${seat.seatName}<strong>(${seat.licensePlate})</strong>
                <br/>
                <strong>From </strong>${seat.startLocation}<strong> to </strong>${seat.endLocation}
                <br/>
                <strong>Start at: </strong>${seat.startDate}
                <br/>
                <strong>Arrival at: </strong>${seat.arrivalDate}
                <br/>
                <strong id="timer_${seat.seatId}" class="text-danger"></strong>
                <br/>
                <button class="btn-danger" onclick="cancelSeat(${seat.seatId})">Cancel</button>
            </div>`;
            $("#cart").append(newElement);
            setCountdown(`timer_${seat.seatId}`, seat.expireDate);
        });
    }
}

window.cancelSeat = (seatId) => {
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    let seat = seats.filter(s => s.seatId == seatId)[0];
    const getCancelSeatSatusURL = `/api/OBTApi/GetCancelSeatSatus/${seat.seatId}`;
    fetch(getCancelSeatSatusURL)
        .then((res) => res.json())
        .then((status) => {
            const newElement = `<img id="seat_${seat.seatId}"
                                     src="/Content/Images/available_seat.gif"
                                     onclick="selectSeat(
                                     ${seat.seatId},
                                    '${seat.seatName}',
                                    '${seat.busType}',
                                     ${seat.journeyId},
                                    '${seat.startLocation}',
                                    '${seat.startDate}',
                                    '${seat.endLocation}',
                                    '${seat.arrivalDate}',
                                    '${seat.licensePlate}',
                                     ${seat.distance},
                                     ${seat.price})" />`
            $(`#seat_${seatId}`).replaceWith(newElement);
            localStorage.setItem("seats", JSON.stringify(seats.filter((seat) => seat.seatId !== seatId)));
            updateCart();
        });
}

window.selectSeat = (seatId, seatName, busType, journeyId, startLocation, startDate, endLocation, arrivalDate, licensePlate, distance, price) => {
    const getSeatStatusURL = `/api/OBTApi/GetSeatStatus/${seatId}?journeyId=${journeyId}&startDate=${startDate}`;
    fetch(getSeatStatusURL)
        .then((res) => res.json())
        .then((status) => { 
            if (status) {
                $(`#seat_${seatId}`).replaceWith(`<img id="seat_${seatId}" src="/Content/Images/booked_seat.gif"/>`);
                return;
            } else {
                let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
                let expireDate = new Date((new Date).getTime() + 300000);
                seats.push({
                    "seatId": seatId,
                    "seatName": seatName,
                    "busType": busType,
                    "journeyId": journeyId,
                    "startLocation": startLocation,
                    "startDate": startDate,
                    "endLocation": endLocation,
                    "arrivalDate": arrivalDate,
                    "licensePlate": licensePlate,
                    "distance": distance,
                    "price": price,
                    "caculatePrice": price,
                    "expireDate": expireDate
                });
                localStorage.setItem("seats", JSON.stringify(seats));
                $(`#seat_${seatId}`).replaceWith(`<img id="seat_${seatId}" src="/Content/Images/selected_seat.gif" onclick="cancelSeat(${seatId})"/>`);      
                updateCart();
            } 
        });
}

document.getElementById("cart-checkout").addEventListener("click", function () {
    updateCart();
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    if (seats.length < 1) {
        alert("Your cart is empty");
    } else {
        window.location.href = "/Home/Checkout";
    }
});

window.initPage = () => {
    updateCart();
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    seats.forEach((seat) => {
        let newElement = `<img id="seat_${seat.seatId}" src="/Content/Images/selected_seat.gif" onclick="cancelSeat(${seat.seatId})"/>`;
        $(`#seat_${seat.seatId}`).replaceWith(newElement);
    });
};