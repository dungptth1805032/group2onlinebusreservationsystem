﻿window.expiredTicketRemove = () => {
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    seats = seats.filter((seat) => {
        const time = (new Date(seat.expireDate).getTime()) - (new Date().getTime());
        return time > 0 ? true : false;
    });
    localStorage.setItem("seats", JSON.stringify(seats));
}

window.setCountdown = (elementId, expireDate) => {
    const deadline = new Date(expireDate).getTime();
    const timer = setInterval(function () {
        const time = deadline - (new Date().getTime());
        if (time > 0) {
            const minutes = Math.floor((time % (1000 * 60 * 60)) / (1000 * 60));
            const seconds = Math.floor((time % (1000 * 60)) / 1000);
            const newElement = `${minutes}m ${seconds}s before expire`;
            try {
                document.getElementById(elementId).innerHTML = newElement;
            } catch {
                clearInterval(timer);
            }
        } else {
            clearInterval(timer);
            document.getElementById(elementId).innerHTML = "Expire";
            cancelSeat(elementId.substring(6));
        }
    }, 1000);
};

window.cancelSeat = (seatId) => {
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    let seat = seats.filter(s => s.seatId == seatId)[0];
    const getCancelSeatSatusURL = `/api/OBTApi/GetCancelSeatSatus/${seat.seatId}`;
    fetch(getCancelSeatSatusURL)
        .then((res) => res.json())
        .then((status) => {
            $(`#seat_${seatId}`).empty();
            seats = seats.filter((seat) => seat.seatId != seatId);
            localStorage.setItem("seats", JSON.stringify(seats));
            if (seats.length < 1) {
                window.location.href = "/";
                return;
            } else {
                seats.forEach((seat) => {
                    updatePrice(seat.seatId);
                });
            }
        });
}

window.updatePrice = (seatId) => {
    let inputValue = document.getElementById(`inputAge_${seatId}`).value;
    let output = !inputValue ? 18 : inputValue;
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    let totalPrice = 0;
    seats.forEach((seat) => {
        if (seat.seatId == seatId && output < 5) {
            seat.caculatePrice = 0;
        } else if (seat.seatId == seatId && output >= 5 && output <= 12) {
            seat.caculatePrice = seat.price / 2;
        }  else if (seat.seatId == seatId && output > 12 && output <= 50) {
            seat.caculatePrice = seat.price;
        } else if (seat.seatId == seatId && output > 50) {
            seat.caculatePrice = seat.price * 0.7;
        }
        if (seat.seatId == seatId) {
            let newElement = seat.caculatePrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            document.getElementById(`price_${seatId}`).innerHTML = newElement;
        }
        totalPrice += seat.caculatePrice;
    });
    totalPrice = totalPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    document.getElementById(`total-price`).innerHTML = totalPrice;
    localStorage.setItem("seats", JSON.stringify(seats));
    document.getElementById(`hiddenField`).value = JSON.stringify(seats);
}

window.initPage = () => {
    expiredTicketRemove();
    const seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    if (seats.length < 1) {
        window.location.href = "/";
        return;
    }
    seats.forEach((seat) => {
        const newElement = `
        <tr id="seat_${seat.seatId}">
            <td>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input id="inputName_${seat.seatId}" class="form-control" name="inputNameIn_${seat.seatId}" type="text" placeholder="Full name" required />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input id="inputAge_${seat.seatId}" class="form-control" name="inputAgeIn_${seat.seatId}" type="number" placeholder="Age" required min="0" max="100" onchange="updatePrice(${seat.seatId})"/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input id="inputID_${seat.seatId}" class="form-control" name="inputIDIn_${seat.seatId}" type="text" placeholder="Citizenship ID" required minlength="6"/>
                    </div>
                </div>
            </td>
            <td>
                <div class="mb-1">
                    <strong>Seat:</strong> ${seat.seatName} <strong>(${seat.licensePlate})</strong>
                    <br />
                    <strong>From</strong> ${seat.startLocation} <strong>to</strong> ${seat.endLocation}
                    <br />
                    <strong>Start at:</strong> ${seat.startDate}
                    <br />
                    <strong>Arrival at:</strong> ${seat.arrivalDate}
                    <br />
                    <strong id="timer_${seat.seatId}" class="text-danger"></strong>
                    <br />
                    <button class="btn-danger" onclick="cancelSeat(${seat.seatId})">Cancel</button>
                </div>
            </td>
            <td>
                <strong>Distance:</strong> ${seat.distance} Kilometer
                <br />
                <strong>Discount:</strong>
                <br />
                - Infant (< 5 year old): Free
                <br/>
                - Child (5 to 12 year old): Half charge
                <br/>
                - Adult (> 12 year old): Full charge
                <br />
                - Elder (> 50 year old): 30% discount
                <br />
                <strong>Your price:</strong> <span id="price_${seat.seatId}"></span> VND
            </td>
        </tr>`;
        $("#seats").append(newElement);
        setCountdown(`timer_${seat.seatId}`, seat.expireDate);
        updatePrice(seat.seatId);
    });
}

initPage();