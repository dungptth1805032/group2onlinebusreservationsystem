﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Group2OnlineBusReservationSystem.Startup))]
namespace Group2OnlineBusReservationSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
