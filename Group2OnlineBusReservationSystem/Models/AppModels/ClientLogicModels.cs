﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Group2OnlineBusReservationSystem.Models.AppModels
{
    public class ClientLogicModels
    {
        private static G2OnlineBusTicketEntities db
        {
            get { return DatabaseConnect.getInstance(); }
        }
        public static List<JourneyDetailResult> SearchForJourneys(JourneySearch searchQuery)
        {
            var result = new List<JourneyDetailResult>();
            try
            {
                var startDate = searchQuery.startDate;
                var startDateLimit = searchQuery.startDate.AddDays(1);
                var journeys = db.Journeys
                    .Where(j => (j.start_time >= startDate && j.start_time <= startDateLimit) || (j.type == "Daily" && j.start_time <= startDate && j.arrival_time >= startDate))
                    .Where(j => j.Bus.type == searchQuery.busType)
                    .Where(j => j.Routes.Where(r => r.Station.name == searchQuery.startLocation).Count() > 0)
                    .Where(j => j.Routes.Where(r => r.Station.name == searchQuery.endLocation).Count() > 0)
                    .Where(j => j.Routes.Where(r => r.Station.name == searchQuery.startLocation).FirstOrDefault().id < j.Routes.Where(r => r.Station.name == searchQuery.endLocation).FirstOrDefault().id)
                    .ToList();
                foreach (var journey in journeys)
                {
                    result.Add(GetJourneyDetail(journey, searchQuery));
                }
            }
            catch { }
            return result;
        }

        public static decimal GetPrice(string busType, decimal distance)
        {
            decimal result = 0;
            var price = db.Prices.Where(p => p.bus_type == busType).ToList();
            if (price != null)
            {
                result = distance * price.FirstOrDefault().price1;
            }
            return result;
        }

        public static JourneyDetailResult GetJourneyDetail(Journey journey, JourneySearch searchQuery)
        {
            var result = new JourneyDetailResult(journey);
            var startDate = searchQuery.startDate.Date.Add(journey.start_time.TimeOfDay);
            var arrivalDate = searchQuery.startDate.Date.Add(journey.start_time.TimeOfDay);
            var startDateStatus = true;
            var arrivalDateStatus = true;
            var distance = 0;
            var incremental = false;
            foreach (var route in journey.Routes)
            {
                startDate = startDateStatus ? startDate.Add(route.time_from_previous_station) : startDate;
                arrivalDate = arrivalDateStatus ? arrivalDate.Add(route.time_from_previous_station) : arrivalDate;
                distance = incremental ? distance += route.distance_from_previous_station : distance;
                if (route.Station.name == searchQuery.startLocation)
                {
                    startDateStatus = false;
                    incremental = true;
                }
                if (route.Station.name == searchQuery.endLocation)
                {
                    arrivalDateStatus = false;
                    incremental = false;
                }
            }
            result.startDate = startDate;
            result.arrivalDate = arrivalDate;
            result.startLocation = searchQuery.startLocation;
            result.endLocation = searchQuery.endLocation;
            result.distance = distance;
            result.price = GetPrice(searchQuery.busType, distance);
            return result;
        }

        public static bool IsSeatBooked(int seatId, int journeyId, DateTime startDate)
        {
            try
            {
                var seat = db.Seats.Find(seatId);
                if (seat.expired_at > DateTime.Now)
                {
                    return true;
                }
                var startDateLimit = startDate.AddDays(1);
                var ticket = db.Tickets
                    .Where(t => t.seat_id == seatId)
                    .Where(t => t.BookingDetail.journey_id == journeyId)
                    .Where(t => t.BookingDetail.start_time >= startDate && t.BookingDetail.start_time <= startDateLimit)
                    .ToList();
                if (ticket.Count() > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return true;
            }
        }

        public static List<List<SeatResult>> GetSeatGrid(int journeyId, DateTime startDate)
        {
            var result = new List<List<SeatResult>>();
            try
            {
                var temporaryList = new List<SeatResult>();
                var journey = db.Journeys.Find(journeyId);
                foreach (var seat in journey.Bus.Seats)
                {
                    var resultItem = new SeatResult
                    {
                        id = seat.id,
                        name = seat.name,
                        busId = seat.bus_id,
                        expiredAt = seat.expired_at,
                        isBooked = IsSeatBooked(seat.id, journeyId, startDate)
                    };
                    if (temporaryList.Count() < 4)
                    {
                        temporaryList.Add(resultItem);
                    }
                    else
                    {
                        result.Add(new List<SeatResult>(temporaryList));
                        temporaryList.Clear();
                        temporaryList.Add(resultItem);
                    }
                }
            }
            catch { }
            return result;
        }

        public static List<Ticket> SaveBookingInformation(FormCollection form)
        {
            var result = new List<Ticket>();
            try
            {
                var bookerId = form["inputBookerIDIn"];
                var customers = db.Customers.Where(customer => customer.citizenship_id == bookerId).ToList();
                var customerId = customers.Count() > 0 ? customers.FirstOrDefault().id : -1;
                if (customerId < 0)
                {
                    var newCustomer = new Customer();
                    newCustomer.name = form["inputBookerNameIn"];
                    newCustomer.address = form["inputBookerAddressIn"];
                    newCustomer.citizenship_id = form["inputBookerIDIn"];
                    newCustomer.email = form["inputBookerEmailIn"];
                    newCustomer.phone = form["inputBookerPhoneIn"];
                    db.Customers.Add(newCustomer);
                    db.SaveChanges();
                    customerId = newCustomer.id;
                }

                var seats = form["hiddenField"].TrimEnd(',');
                var seatsJson = JsonConvert.DeserializeObject<List<SeatModel>>(seats);
                var totalPrice = 0;
                foreach (var seat in seatsJson)
                {
                    totalPrice += seat.caculatePrice;
                }

                var newPaymentDetail = new PaymentDetail();
                newPaymentDetail.type = form["paymentOption"];
                newPaymentDetail.amount = totalPrice;
                newPaymentDetail.status = "pending";
                newPaymentDetail.canceled = false;
                newPaymentDetail.refund = 0;
                db.PaymentDetails.Add(newPaymentDetail);
                db.SaveChanges();

                var startTime = seatsJson.FirstOrDefault().startDate;
                var arrivalTime = seatsJson.FirstOrDefault().arrivalDate;

                var newBookingDetail = new BookingDetail();
                newBookingDetail.customer_id = customerId;
                newBookingDetail.journey_id = seatsJson.FirstOrDefault().journeyId;
                newBookingDetail.from_location = seatsJson.FirstOrDefault().startLocation;
                newBookingDetail.to_location = seatsJson.FirstOrDefault().endLocation;
                newBookingDetail.start_time = Convert.ToDateTime(startTime);
                newBookingDetail.arrival_time = Convert.ToDateTime(arrivalTime);
                newBookingDetail.payment_detail_id = newPaymentDetail.id;
                newBookingDetail.status = true;
                db.BookingDetails.Add(newBookingDetail);
                db.SaveChanges();

                foreach (var seat in seatsJson)
                {
                    var newTicket = new Ticket();
                    newTicket.booking_detail_id = newBookingDetail.id;
                    newTicket.customer_name = form["inputNameIn_" + seat.seatId + ""];
                    newTicket.citizenship_id = form["inputIDIn_" + seat.seatId + ""];
                    newTicket.age = Convert.ToInt32(form["inputAgeIn_" + seat.seatId + ""]);
                    newTicket.price = seat.caculatePrice;
                    newTicket.seat_id = seat.seatId;
                    db.Tickets.Add(newTicket);
                    db.SaveChanges();
                    result.Add(newTicket);
                }
                db.SaveChanges();
            }
            catch { }
            return result;
        }
    }
}