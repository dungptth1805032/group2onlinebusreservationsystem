﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Group2OnlineBusReservationSystem.Models.AppModels
{
    public class JourneySearch
    {
        [Display(Name = "From")]
        public string startLocation { get; set; }

        [Display(Name = "To")]
        public string endLocation { get; set; }

        [Display(Name = "Bus type")]
        public string busType { get; set; }

        [Display(Name = "Departure")]
        public DateTime startDate { get; set; }
    }

    public class TicketsSearch
    {
        public int bookingId { get; set; }
        public int ticketId { get; set; }
        public string phone { get; set; }
    }
}