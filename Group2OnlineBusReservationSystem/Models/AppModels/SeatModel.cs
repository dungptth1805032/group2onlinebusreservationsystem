﻿using System;

namespace Group2OnlineBusReservationSystem.Models.AppModels
{
    public class SeatModel
    {
        public int seatId { get; set; }
        public string seatName { get; set; }
        public string busType { get; set; }
        public int journeyId { get; set; }
        public string startLocation { get; set; }
        public DateTime startDate { get; set; }
        public string endLocation { get; set; }
        public DateTime arrivalDate { get; set; }
        public string licensePlate { get; set; }
        public int distance { get; set; }
        public int price { get; set; }
        public int caculatePrice { get; set; }
        public DateTime expireDate { get; set; }
    }
}