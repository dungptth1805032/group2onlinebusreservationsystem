﻿namespace Group2OnlineBusReservationSystem.Models.AppModels
{
    public class DatabaseConnect
    {
        public static G2OnlineBusTicketEntities db { get; set; }
        public static G2OnlineBusTicketEntities getInstance()
        {
            if (db == null)
            {
                db = new G2OnlineBusTicketEntities();
                return db;
            }
            return db;
        }
    }
}